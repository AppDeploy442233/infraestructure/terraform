variable "name-rg" {
  type = string
  default = "CAMBIAMENAMERG"
}

variable "nameofcluster" {
  type = string
  default = "CAMBIAMENAMEOFCLUSTER"
}


variable "publicip" {
  type = string
  default = "CAMBIAMEPUBLICIP"
}

