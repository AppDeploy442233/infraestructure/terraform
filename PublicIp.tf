
resource "azurerm_public_ip" "publicip" {
  name                = "${var.publicip}"
  resource_group_name = azurerm_resource_group.rg-01.name
  location            = azurerm_resource_group.rg-01.location
  allocation_method   = "Static"

}