resource "azurerm_virtual_network" "example-1" {
  name                = "example-network-${var.name-rg}"
  resource_group_name = "${azurerm_resource_group.rg-01.name}"
  location            = "${azurerm_resource_group.rg-01.location}"
  address_space       = ["10.200.0.0/22"]
}

resource "azurerm_subnet" "frontend" {
  name                 = "frontend"
  resource_group_name  = "${azurerm_resource_group.rg-01.name}"
  virtual_network_name = azurerm_virtual_network.example-1.name
  address_prefixes     = ["10.200.0.0/24"]
}

resource "azurerm_subnet" "backend" {
  name                 = "backend"
  resource_group_name  = "${azurerm_resource_group.rg-01.name}"
  virtual_network_name = azurerm_virtual_network.example-1.name
  address_prefixes     = ["10.200.2.0/24"]
}

resource "azurerm_public_ip" "example" {
  name                = "example-pip-${var.name-rg}"
  resource_group_name = "${azurerm_resource_group.rg-01.name}"
  location            = "${azurerm_resource_group.rg-01.location}"
  sku                 = "Standard"
  allocation_method   = "Static"
}

#&nbsp;since these variables are re-used - a locals block makes this more maintainable
locals {
  backend_address_pool_name      = "${azurerm_virtual_network.example-1.name}-beap"
  frontend_port_name             = "${azurerm_virtual_network.example-1.name}-feport"
  frontend_ip_configuration_name = "${azurerm_virtual_network.example-1.name}-feip"
  http_setting_name              = "${azurerm_virtual_network.example-1.name}-be-htst"
  listener_name                  = "${azurerm_virtual_network.example-1.name}-httplstn"
  request_routing_rule_name      = "${azurerm_virtual_network.example-1.name}-rqrt"
  redirect_configuration_name    = "${azurerm_virtual_network.example-1.name}-rdrcfg"
}

resource "azurerm_application_gateway" "network" {
  name                = "example-appgateway-${var.name-rg}"
  resource_group_name = "${azurerm_resource_group.rg-01.name}"
  location            = "${azurerm_resource_group.rg-01.location}"

  sku {
    name     = "Standard_v2"
    tier     = "Standard_v2"
    capacity = 2

  }

  gateway_ip_configuration {
    name      = "my-gateway-ip-configuration"
    subnet_id = azurerm_subnet.frontend.id
  }

  frontend_port {
    name = local.frontend_port_name
    port = 80
  }

  frontend_ip_configuration {
    name                 = local.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.example.id
  }

  backend_address_pool {
    name = local.backend_address_pool_name
  }

  backend_http_settings {
    name                  = local.http_setting_name
    cookie_based_affinity = "Disabled"
    path                  = "/"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 60
  }

  http_listener {
    name                           = local.listener_name
    frontend_ip_configuration_name = local.frontend_ip_configuration_name
    frontend_port_name             = local.frontend_port_name
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = local.request_routing_rule_name
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name
    backend_address_pool_name  = local.backend_address_pool_name
    backend_http_settings_name = local.http_setting_name
  }
}