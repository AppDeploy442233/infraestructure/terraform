resource "azurerm_virtual_network_peering" "example-1-1" {
  name                      = "peer1to2"
  resource_group_name       = "${azurerm_resource_group.rg-01.name}"
  virtual_network_name      = "${azurerm_virtual_network.example-1.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.example-2.id}"

 depends_on = [
    azurerm_virtual_network.example-1,
    azurerm_virtual_network.example-2,
  ] 

}


resource "azurerm_virtual_network_peering" "example-2-1" {
  name                      = "peer2to1"
  resource_group_name       = "${azurerm_resource_group.rg-01.name}"
  virtual_network_name      = "${azurerm_virtual_network.example-2.name}"
  remote_virtual_network_id = "${azurerm_virtual_network.example-1.id}"

depends_on = [
  azurerm_virtual_network.example-1,
  azurerm_virtual_network.example-2,
  ] 

}
