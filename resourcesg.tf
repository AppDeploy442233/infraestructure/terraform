# Create a resource group
resource "azurerm_resource_group" "rg-01" {
  name     = "${var.name-rg}"
  location = "eastus"
}