resource "azurerm_virtual_network" "example-2" {
  name                = "${var.name-rg}-vnet"
  location            = "${azurerm_resource_group.rg-01.location}"
  resource_group_name = "${azurerm_resource_group.rg-01.name}"
  address_space       = ["120.10.0.0/22"]
}

resource "azurerm_subnet" "example" {
  name                 = "${var.name-rg}-subnet"
  resource_group_name  = "${azurerm_resource_group.rg-01.name}"
  address_prefixes     = ["120.10.0.0/22"]
  virtual_network_name = azurerm_virtual_network.example-2.name
}


resource "azurerm_kubernetes_cluster" "aks01" {
  name                = "${var.nameofcluster}"
  location            = "${azurerm_resource_group.rg-01.location}"
  resource_group_name = "${azurerm_resource_group.rg-01.name}"
  dns_prefix          = "ejemplo01"
  default_node_pool {
    name       = "pool01"
    node_count = 1
    vm_size    = "Standard_D2_v2"
    vnet_subnet_id = azurerm_subnet.example.id
  }

  identity {
    type = "SystemAssigned"
  }

  addon_profile {
  oms_agent {
   enabled                    = true
   log_analytics_workspace_id = "${azurerm_log_analytics_workspace.workspacesyd.id}"
  }
  }



 depends_on = [
    azurerm_container_registry.acr,
  ] 

}

