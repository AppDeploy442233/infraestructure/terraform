resource "azurerm_log_analytics_workspace" "workspacesyd" {
  name                = "workspacesyd"
  location            = azurerm_resource_group.rg-01.location
  resource_group_name = azurerm_resource_group.rg-01.name
  sku                 = "PerGB2018"
  retention_in_days   = 30
}