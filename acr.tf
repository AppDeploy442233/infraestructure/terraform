resource "azurerm_container_registry" "acr" {
  name                = "appdeployyd001"
  resource_group_name = azurerm_resource_group.rg-01.name
  location            = azurerm_resource_group.rg-01.location
  sku                 = "Basic"
  admin_enabled       = true
}

